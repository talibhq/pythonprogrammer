from django.conf.urls import url
from pythonprogrammer import views


app_name = 'pythonprogrammer'

urlpatterns = [
    url(r'^details/', views.details,name='details'),
    url(r'^libraries/', views.libraries,name='libraries'),
    url(r'^signup/', views.signup,name='signup'),
    url(r'^login/', views.user_login,name='user_login'),
    url(r'^user_logout/', views.user_logout,name='user_logout'),
    url(r'^profile/', views.profile,name='profile'),
    url(r'^update_profile/', views.update_profile,name='update_profile'),
 ]

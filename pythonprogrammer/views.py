from django.shortcuts import render,redirect
from .forms import Signup_Form,Login_Form,Update_Profile_Form
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages

# Create your views here.

def home(request):
	return render(request,'pythonprogrammer/home.html')

def details(request):
	return render(request,'pythonprogrammer/details.html')

def libraries(request):
	return render(request,'pythonprogrammer/libraries.html')

def signup(request):
    if request.method == 'POST':
        form = Signup_Form(request.POST)
        if form.is_valid():
            user = form.save()
            user.set_password(user.password)
            user.save()
            messages.success(request,'Signup Successfull Thank You!!')
            return redirect('/pythonprogrammer/login')
        else:
            return render(request,'pythonprogrammer/signup.html',{'form':form})
    else:
        form = Signup_Form()
        context = {'form':form}
        return render(request,'pythonprogrammer/signup.html',context)

def user_login(request):
	form = Login_Form()
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(username=username,password=password)

		if user:
			if user.is_active:
				login(request,user)
				return redirect('home')
			else:
				return redirect('/pythonprogrammer/login',{'form':form})
		else:
			messages.warning(request,'Wrong Details!!')
			return redirect('/pythonprogrammer/login',{'form':form})
	else:
		return render(request,'pythonprogrammer/login.html',{'form':form})

@login_required
def user_logout(request):
	logout(request)
	return redirect('home')

def profile(request):
	user = request.user
	return render(request,'pythonprogrammer/profile.html',{'user':user})

def update_profile(request):
	form = Update_Profile_Form(instance=request.user)
	if request.method == 'POST':
		form = Update_Profile_Form(request.POST,instance=request.user)
		if form.is_valid():
			form.save()
			messages.success(request,'Your profile has been updated!!')
			return redirect('/pythonprogrammer/profile')
		else:
			return render(request,'pythonprogrammer/update_profile.html',{'form':form})
	else:
		return render(request,'pythonprogrammer/update_profile.html',{'form':form})
		
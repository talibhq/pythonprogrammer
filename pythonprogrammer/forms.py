from django import forms
from django.contrib.auth.models import User
import re
from django.core.validators import validate_email
from django.contrib.auth.forms import UserChangeForm

class Signup_Form(forms.ModelForm):
	username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Username..'}))
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter First Name..'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Last Name..'}))
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'Enter Email..'}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Enter Password..'}))
	cnfpassword = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Enter Password Again..'}))


	class Meta:
		model = User
		fields = ['username','first_name','last_name','email','password']

	def clean_username(self):
		username = self.cleaned_data['username']
		if len(username) >= 6 and len(username) <= 12:
			try:
				User.objects.get(username=username)
				raise forms.ValidationError('username already taken')
			except:
				regex = '^[a-z](?=.*[0-9][0-9][0-9])[a-z0-9]+$'
				match = re.fullmatch(regex,username)
				if match != None:
					return username
				else:
					raise forms.ValidationError('invalid username')
		else:
			raise forms.ValidationError('invalid username')

	def clean_first_name(self):
		first_name = self.cleaned_data['first_name']
		if first_name.isalpha():
			return first_name
		else:
			raise forms.ValidationError('a good name only contain alphabate')

	def clean_last_name(self):
		last_name = self.cleaned_data['last_name']
		if last_name.isalpha():
			return last_name
		else:
			raise forms.ValidationError('a good name only contain alphabate')

	def clean_email(self):
		email = self.cleaned_data['email']
		try:
			valid = validate_email(email)
		except:
			raise forms.ValidationError('Invalid Email')
		try:
			match = User.objects.get(email=email)
		except:
			return email
			raise forms.ValidationError('Email Already Exist')

	def clean_password(self):
		password = self.cleaned_data['password']
		regex = '^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-z0-9!@#$%^&*]{8,15}$'
		match = re.fullmatch(regex,password)
		if match != None:
			return password
		else:
			raise forms.ValidationError('Invalid password')

	def clean_cnfpassword(self):
		password = self.cleaned_data.get('password')
		cnfpassword = self.cleaned_data['cnfpassword']
		if cnfpassword == password:
			return cnfpassword
		else:
			raise forms.ValidationError('password not matched')

class Login_Form(forms.ModelForm):
	username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Username..'}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Enter Password..'}))

	class Meta:
		model = User
		fields = ['username','password']

class Update_Profile_Form(UserChangeForm):

	username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Username..'}))
	first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter First Name..'}))
	last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Enter Last Name..'}))
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control','placeholder':'Enter Email..'}))

	class Meta:
		model = User
		fields = ['username','first_name','last_name','email','password']

	def clean_username(self):
		username = self.cleaned_data['username']
		if len(username) >= 6 and len(username) <= 12:
			try:
				User.objects.get(username=username)
				raise forms.ValidationError('username already taken')
			except:
				regex = '^[a-z](?=.*[0-9][0-9][0-9])[a-z0-9]+$'
				match = re.fullmatch(regex,username)
				if match != None:
					return username
				else:
					raise forms.ValidationError('invalid username')
		else:
			raise forms.ValidationError('invalid username')

	def clean_first_name(self):
		first_name = self.cleaned_data['first_name']
		if first_name.isalpha():
			return first_name
		else:
			raise forms.ValidationError('a good name only contain alphabate')

	def clean_last_name(self):
		last_name = self.cleaned_data['last_name']
		if last_name.isalpha():
			return last_name
		else:
			raise forms.ValidationError('a good name only contain alphabate')

	def clean_email(self):
		email = self.cleaned_data['email']
		try:
			valid = validate_email(email)
		except:
			raise forms.ValidationError('Invalid Email')
		try:
			match = User.objects.get(email=email)
		except:
			return email
			raise forms.ValidationError('Email Already Exist')

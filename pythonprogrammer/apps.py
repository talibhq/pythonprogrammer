from django.apps import AppConfig


class PythonprogrammerConfig(AppConfig):
    name = 'pythonprogrammer'
